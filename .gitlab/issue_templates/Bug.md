When reporting a bug, please make sure

* Your Eigen version is up-to-date!
* You provide all steps necessary to reproduce the bug, i.e., a self-contained
  example, as well as the compiler version(s) you tested and compile flags you 
  are using.
* Describe the expected and actual behavior
* Use code blocks (```) for short listings. For long listings (of source or 
  compiler errors/warnings) prefer using the attachment feature.
* If you are unsure if it is a bug or a usage error, you may want to ask first
  at https://stackoverflow.com/, at our mailing list, or in our IRC channel.
* (optionally) If you already investigated what the cause of the bug (might) be,
  or have a possible fix, please share that information, of course.
* Remove this list from the actual bug report :)

/label ~bug 
